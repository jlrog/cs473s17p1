# Facility Management System 

**System requirements**

* Java 8 SDK or later

**Running the java JAR Application**

From the source directory go to the location of the artifact folder:

`out/artifacts/facility_jar/Facility.jar`

Then run the app with the java compiler and the classpath:

`java -cp  edu/luc/cs473/system/view/Main`

```sh
java -cp Facility.jar edu/luc/cs473/facility/view/Main

 Select an option from the menu:

 	 1) Test Facility Client

 	 2) Test Use Client

 	 3) Test Maintenance Client

 	 4) Quit
```

## Facility Management Client Test

### Test Facility Client
![](doc/test1.png)

### Test Use Client
![](doc/test2.png)

### Test Maintenance Client
![](doc/test3.png)

## Project Diagram UML

### Composite Structure
![](doc/systemDiagram.png)

## PostgreSQL Tables Structure

```sh
psql (9.6.2, server 9.6.1)
facilitydb=> \d
                     List of relations
 Schema |             Name              |   Type   | Owner
--------+-------------------------------+----------+--------
 public | facility                      | table    | devops
 public | facility_details              | table    | devops
 public | facility_facility_id_seq      | sequence | devops
 public | facility_use                  | table    | devops
 public | facility_use_order_number_seq | sequence | devops
 public | inspection                    | table    | devops
 public | maintenance                   | table    | devops
(7 rows)

facilitydb=> \d facility
                 Table "facility"
    Column     |  Type   |         Modifiers
---------------+---------+-------------------------------
 facility_id   | integer | not null default nextval()
 facility_name | text    |
 room_number   | integer |
 media         | boolean |
 max_capacity  | integer |

facilitydb=> \d facility_details
    Table "facility_details"
     Column      |  Type   | Modifiers
-----------------+---------+-----------
 facility_id     | integer |
 phone_number    | text    |
 department      | text    |
 occupied        | boolean |
 inspection_date | date    |

facilitydb=> \d facility_use
              Table "facility_use"
      Column       |  Type   |         Modifiers
-------------------+---------+---------------------------
 order_number      | integer | not null default nextval()
 facility_id       | integer |
 customer_id       | integer | not null
 reservation_start | date    |
 reservation_end   | date    |
 inspection_date   | date    |

facilitydb=> \d inspection
        Table "inspection"
      Column       |  Type   | Modifiers
-------------------+---------+-----------
 facility_id       | integer |
 inspection_code   | text    |
 inspection_date   | date    |
 passed_inspection | boolean |
 description       | text    |

facilitydb=> \d maintenance
        Table "maintenance"
       Column       |  Type   | Modifiers
--------------------+---------+-----------
 facility_id        | integer |
 start_work         | date    |
 end_work           | date    |
 facility_downtime  | numeric |
 maintenance_cost   | integer |
 maintenance_log    | text    |
 maintenance_status | text    |

```