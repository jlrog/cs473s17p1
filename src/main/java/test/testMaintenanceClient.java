package test;

import edu.luc.cs473.facility.models.maintenance.Maintenance;
import edu.luc.cs473.facility.service.MaintenanceService;
import java.time.LocalDate;
import java.util.List;

/**
 * Created by jlroo on 2/20/17.
 */

public class testMaintenanceClient {

    public testMaintenanceClient() throws Exception {

        MaintenanceService maintenanceService = new MaintenanceService();

        Maintenance order01 = new Maintenance();
        order01.setFacilityID(1);
        order01.setMaintenanceStart(LocalDate.of(2017,01,01));
        order01.setMaintenanceEnd(LocalDate.of(2017,01,05));
        order01.setFacilityDowntime(order01.getMaintenanceStart(),order01.getMaintenanceEnd());
        order01.setMaintenanceCost(2500);
        order01.setMaintenanceLog("AC damaged, TV not working, computer broken");
        maintenanceService.addMaintenance(order01);

        Maintenance order02 = new Maintenance();
        order02.setFacilityID(3);
        order02.setMaintenanceStart(LocalDate.of(2017,01,05));
        order02.setMaintenanceEnd(LocalDate.of(2017,01,15));
        order02.setFacilityDowntime(order02.getMaintenanceStart(),order02.getMaintenanceEnd());
        order02.setMaintenanceCost(2000);
        order02.setMaintenanceLog("Computer not working");
        maintenanceService.addMaintenance(order02);

        Maintenance order03 = new Maintenance();
        order03.setFacilityID(2);
        order03.setMaintenanceStart(LocalDate.of(2017,03,10));
        order03.setMaintenanceEnd(LocalDate.of(2017,04,22));
        order03.setFacilityDowntime(order03.getMaintenanceStart(),order03.getMaintenanceEnd());
        order03.setMaintenanceCost(3000);
        order03.setMaintenanceLog("water problem");
        maintenanceService.addMaintenance(order03);
        maintenanceService.addMaintenance(order03);
        maintenanceService.addMaintenance(order03);

        Maintenance order04 = new Maintenance();
        order04.setFacilityID(1);
        order04.setMaintenanceStart(LocalDate.of(2016,12,10));
        order04.setMaintenanceEnd(LocalDate.of(2016,12,12));
        order04.setFacilityDowntime(order04.getMaintenanceStart(),order04.getMaintenanceEnd());
        order04.setMaintenanceCost(3000);
        order04.setMaintenanceLog("electrical problem");
        maintenanceService.addMaintenance(order04);
        maintenanceService.addMaintenance(order04);

        System.out.println("[TEST] Maintenance in the system ");
        List<String> maintenance_facilities;
        System.out.println("-------------------------------------------------------");
        maintenance_facilities = maintenanceService.getAllMaintenance();
        for (String line: maintenance_facilities){
            System.out.println( line );
        }
        System.out.println("------------------------------------------------------- ");


        System.out.println("[TEST] Maintenance Client: Get individual Maintenance 01 ");
        String readMaintenance = maintenanceService.getMaintenance(1);
        System.out.print(readMaintenance+"");

        System.out.println("\n[TEST] Maintenance Client: Delete Facility 02 ");
        maintenanceService.deleteMaintenance(3);

        System.out.println("[TEST] Maintenance in the system ");
        System.out.println("------------------------------------------------------- ");
        maintenance_facilities = maintenanceService.getAllMaintenance();
        for (String line: maintenance_facilities){
            System.out.println( line );
        }
        System.out.println("------------------------------------------------------- ");

        System.out.println("[TEST] Maintenance Client: Add and Update a Facility ");

        order03.setMaintenanceStart(LocalDate.of(2017,03,10));
        order03.setMaintenanceEnd(LocalDate.of(2017,03,15));
        order03.setFacilityDowntime(order03.getMaintenanceStart(),order03.getMaintenanceEnd());
        order03.setMaintenanceCost(1000);
        order03.setMaintenanceLog("water problem - fixed");
        maintenanceService.updateMaintenance(order03,2);

        System.out.println("[TEST] Maintenance in the system ");
        System.out.println("------------------------------------------------------- ");
        maintenance_facilities = maintenanceService.getAllMaintenance();
        for (String line: maintenance_facilities){
            System.out.println( line );
        }
        System.out.println("------------------------------------------------------- ");
    }
}