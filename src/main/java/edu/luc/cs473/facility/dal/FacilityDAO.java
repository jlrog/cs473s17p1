package edu.luc.cs473.facility.dal;

import edu.luc.cs473.facility.models.facility.Details;
import edu.luc.cs473.facility.models.facility.Facility;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by jlroo on 2/20/17.
 */

public class FacilityDAO {

    public FacilityDAO() {}

    private static final String TableName           = "facility";
    private static final String IDColumn            = "facility_id";
    private static final String NameColumn          = "facility_name";
    private static final String roomNumberColumn    = "room_number";
    private static final String mediaColumn         = "media";
    private static final String capacityColumn      = "max_capacity";

    public Collection<Facility> ReadAllFacilities() throws Exception {
        List<Facility> FacilityList = new ArrayList<>();
        Statement conn = dbConnect.getConnection().createStatement();
        try {
            String query = "SELECT * FROM " + TableName ;
            ResultSet response = conn.executeQuery(query);
            while ( response.next() ) {

                int facilityID  = response.getInt(1);
                String name     = response.getString(2);
                int room        = response.getInt(3);
                boolean media   = response.getBoolean(4);
                int capacity    = response.getInt(5);

                Facility FacilityObj = new Facility();
                FacilityObj.setFacilityID(facilityID);
                FacilityObj.setName(name);
                FacilityObj.setRoomNumber(room);
                FacilityObj.setMedia(media);
                FacilityObj.setMaxCapacity(capacity);
                FacilityList.add(FacilityObj);
            }
            response.close();
            conn.close();
        }
        catch (SQLException se) {
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
        return FacilityList;
    }

    public Facility ReadFacility(int facility_ID) throws Exception {
        Statement conn = dbConnect.getConnection().createStatement();
        Facility FacilityObj = new Facility();

        try {
            String query = "SELECT * FROM " + TableName + " WHERE facility_id = " + facility_ID;
            ResultSet response = conn.executeQuery(query);
            while ( response.next() ) {

                int facilityID = response.getInt(1);
                String name = response.getString(2);
                int room = response.getInt(3);
                boolean media = response.getBoolean(4);
                int capacity = response.getInt(5);

                FacilityObj.setFacilityID(facilityID);
                FacilityObj.setName(name);
                FacilityObj.setRoomNumber(room);
                FacilityObj.setMedia(media);
                FacilityObj.setMaxCapacity(capacity);
            }

            response.close();
            conn.close();
        }
        catch (SQLException se) {
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
        return FacilityObj;
    }

    public void Insert(Facility newObj) throws Exception {
        Connection conn = dbConnect.getConnection();
        try {
            String sql = "INSERT INTO " + TableName + " VALUES (?,?,?,?,?)";
            PreparedStatement sqlFacility = conn.prepareStatement(sql);
            sqlFacility.setInt(1, newObj.getFacilityID());
            sqlFacility.setString(2, newObj.getName());
            sqlFacility.setInt(3, newObj.getRoomNumber());
            sqlFacility.setBoolean(4, newObj.isMedia());
            sqlFacility.setInt(5, newObj.getMaxCapacity());
            sqlFacility.executeUpdate();
            conn.commit();

        } catch (SQLException se) {
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
        try {
            String sql = "INSERT INTO facility_details VALUES (?,?,?,?,?)";
            PreparedStatement sqlDetails = conn.prepareStatement(sql);
            Details facilityDetails = newObj.getDetails();
            if (facilityDetails.getPhoneNumber() == null) facilityDetails.setPhoneNumber("");
            if (facilityDetails.getDepartment() == null) facilityDetails.setDepartment("");
            sqlDetails.setInt(1,facilityDetails.getFacilityID() );
            sqlDetails.setString(2, facilityDetails.getPhoneNumber());
            sqlDetails.setString(3, facilityDetails.getDepartment());
            sqlDetails.setBoolean(4, facilityDetails.isOccupied());
            sqlDetails.setDate(5, Date.valueOf(facilityDetails.getInspected()));
            sqlDetails.executeUpdate();
            conn.commit();

        } catch (SQLException se) {
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
        conn.close();
    }

    public void Delete(int deleteID) throws Exception {
        Connection conn = dbConnect.getConnection();
        try {
            String sql = "DELETE FROM " + TableName + " WHERE " + IDColumn +  " = ?" ;
            PreparedStatement sqlDelete = conn.prepareStatement(sql);
            sqlDelete.setInt(1,deleteID);
            sqlDelete.executeUpdate();
            conn.commit();
            conn.close();
        }
        catch (SQLException se) {
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
    }

    public void Update(Facility updateObj,int updateID) throws Exception {

        Connection conn = dbConnect.getConnection();
        try {
            String sql = "UPDATE " + TableName + " SET " +
                    NameColumn          + " = ? , " +
                    roomNumberColumn    + " = ? , " +
                    mediaColumn         + " = ? ," +
                    capacityColumn      + " = ? " +
                    " WHERE " + IDColumn + " = " + updateID;

            PreparedStatement sqlFacility = conn.prepareStatement(sql);
            sqlFacility.setString(1, updateObj.getName());
            sqlFacility.setInt(2, updateObj.getRoomNumber());
            sqlFacility.setBoolean(3, updateObj.isMedia());
            sqlFacility.setInt(4, updateObj.getMaxCapacity());
            sqlFacility.executeUpdate();
            conn.commit();

        } catch (SQLException se) {
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
        try {
            String sql = "UPDATE " + "facility_details" +" SET " +
                            "phone_number = ? , "+
                            "department = ? , " +
                            "occupied = ? , "+
                            "inspection_date = ? " +
                        "WHERE " + IDColumn + " = " + updateID;

            PreparedStatement sqlDetails = conn.prepareStatement(sql);
            Details facilityDetails = updateObj.getDetails();
            if (facilityDetails.getPhoneNumber() == null) facilityDetails.setPhoneNumber("");
            if (facilityDetails.getDepartment() == null) facilityDetails.setDepartment("");
            sqlDetails.setString(1, facilityDetails.getPhoneNumber());
            sqlDetails.setString(2, facilityDetails.getDepartment());
            sqlDetails.setBoolean(3, facilityDetails.isOccupied());
            sqlDetails.setDate(4, Date.valueOf(facilityDetails.getInspected()));
            sqlDetails.executeUpdate();
            conn.commit();

        } catch (SQLException se) {
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
        conn.close();
    }

    public void cleanTables() throws Exception {
        Connection conn = dbConnect.getConnection();
        try {
            String sql = "DELETE FROM facility";
            PreparedStatement response = conn.prepareStatement(sql);
            response.executeUpdate();
            conn.commit();
            conn.close();
        }
        catch (SQLException se) {
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
    }

}
