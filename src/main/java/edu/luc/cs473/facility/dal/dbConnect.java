package edu.luc.cs473.facility.dal;
import java.sql.*;


/**
 * Created by jlroo on 2/20/17.
 */

public class dbConnect {

    static final String JDBC_DRIVER = "org.postgresql.Driver";
    static final String DB_URL = "jdbc:postgresql://facilitydb.cccirrseg79g.us-east-1.rds.amazonaws.com:5432/facilitydb";
    static final String USER = "devops";
    static final String PASS = "comp2017";

    @SuppressWarnings("unused")
    public static Connection getConnection() throws SQLException, ClassNotFoundException {
        try { Class.forName(JDBC_DRIVER); }
        catch (ClassNotFoundException e) { e.printStackTrace();}
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(DB_URL,USER,PASS);
            Statement query = conn.createStatement();
            ResultSet response = query.executeQuery("SELECT VERSION();");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        SetCommit(conn);
        SetIsolationLevel(conn, Connection.TRANSACTION_SERIALIZABLE);
        return conn;
    }

    private static void SetCommit(Connection connection) throws SQLException {
        connection.setAutoCommit(false);
        boolean mode = connection.getAutoCommit();
        if (mode) {
            throw new RuntimeException("Failed to set autocommit");
        }
    }

    private static void SetIsolationLevel(Connection connection, int level) throws SQLException {
        connection.setTransactionIsolation(level);
        int setLevel = connection.getTransactionIsolation();
        if (setLevel != level) {
            throw new RuntimeException("failed to set isolation level");
        }
    }
}
