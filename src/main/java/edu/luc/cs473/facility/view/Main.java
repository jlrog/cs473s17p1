package edu.luc.cs473.facility.view;

import test.testFacilityClient;
import test.testMaintenanceClient;
import test.testUseClient;
import edu.luc.cs473.facility.service.FacilityService;
import java.util.Scanner;


public class Main {

    public static void main(String[] args) throws Exception {

        boolean flag = false;

        do {

            System.out.println("\n Select an option from the menu:");
            System.out.println("\n \t 1) Test Facility Client");
            System.out.println("\n \t 2) Test Use Client");
            System.out.println("\n \t 3) Test Maintenance Client");
            System.out.println("\n \t 4) Quit");

            Scanner scanner = new Scanner(System.in);
            int option = Integer.parseInt(scanner.next());
            FacilityService facilityClean = new FacilityService();

            switch(option) {
                case 1:
                    new testFacilityClient();
                    break;
                case 2:
                    new testUseClient();
                    break;
                case 3:
                    new testMaintenanceClient();
                    break;
                case 4:
                    facilityClean.systemClean();
                    flag = true;
                    break;
                default:
                    System.out.println("Invalid option.");
                    break;
            }
        }while (flag==false);
    }
}
