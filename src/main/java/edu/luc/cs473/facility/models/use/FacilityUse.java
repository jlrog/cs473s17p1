package edu.luc.cs473.facility.models.use;

import edu.luc.cs473.facility.models.facility.Facility;
import lombok.Data;
import java.time.LocalDate;

/**
 * Created by jlroo on 2/20/17.
 */

@Data
public class FacilityUse extends Facility {

    private int orderNumber;
    private LocalDate reservationStart;
    private LocalDate reservationEnd;
    private boolean occupied;
    private int customerID;
    private Inspection inspection;

    public void setInspection(Inspection inspection) {
        this.inspection = inspection;
    }
}
