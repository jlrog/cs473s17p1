package edu.luc.cs473.facility.models.use;
import edu.luc.cs473.facility.models.facility.Facility;
import lombok.Data;

import java.time.LocalDate;

/**
 * Created by jlroo on 2/20/17.
 */

@Data
public class Inspection extends Facility {
    private String inspectionCode;
    private LocalDate inspectionDate;
    private Boolean passedInspection;
    private String description;
}
