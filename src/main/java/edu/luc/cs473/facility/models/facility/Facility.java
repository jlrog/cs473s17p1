package edu.luc.cs473.facility.models.facility;
import lombok.Data;

/**
 * Created by jlroo on 2/20/17.
 */

@Data
public class Facility {
    private int facilityID;
    private String name;
    private int roomNumber;
    private boolean media;
    private int maxCapacity;
    private Details details;

    public void setDetails(Details details) {
        this.details = details;
    }

}
