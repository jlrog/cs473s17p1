package edu.luc.cs473.facility.models.facility;

import lombok.Data;

import java.time.LocalDate;

/**
 * Created by jlroo on 2/20/17.
 */

@Data
public class Details extends Facility {
    private String phoneNumber;
    private String department;
    private boolean occupied;
    private LocalDate inspected;
}
