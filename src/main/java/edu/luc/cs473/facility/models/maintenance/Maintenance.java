package edu.luc.cs473.facility.models.maintenance;

import edu.luc.cs473.facility.models.facility.Facility;
import lombok.Data;
import java.time.Duration;
import java.time.LocalDate;

/**
 * Created by jlroo on 2/20/17.
 */

@Data
public class Maintenance extends Facility {

    private LocalDate maintenanceStart;
    private LocalDate maintenanceEnd;
    private long facilityDowntime;
    private int maintenanceCost;
    private String maintenanceLog;
    private String MaintenanceStatus;

    public void setFacilityDowntime(LocalDate start, LocalDate end){
        this.facilityDowntime = Duration.between( start.atTime(0, 0),end.atTime(0, 0)).toMinutes();
    }

    public void setFacilityDowntime(Long downtime){
        this.facilityDowntime = downtime;
    }
}
