package edu.luc.cs473.facility.service;

import edu.luc.cs473.facility.dal.MaintenanceDAO;
import edu.luc.cs473.facility.models.maintenance.Maintenance;
import edu.luc.cs473.facility.view.Main;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by jlroo on 2/20/17.
 */

public class MaintenanceService {

    private MaintenanceDAO _maintenanceDAO = new MaintenanceDAO();

    public List<String> getAllMaintenance() {
        List<String> facility_maintenance = new ArrayList<>();
        try {
            Collection<Maintenance> All_Orders = _maintenanceDAO.ReadAllMaintenance();
            for (Maintenance facility:All_Orders) {
                String response =  "facility_id: "          + facility.getFacilityID() +
                                    " facility_downtime: "  + facility.getFacilityDowntime() +
                                    "\nmaintenance_cost: "   + facility.getMaintenanceCost() +
                                    " maintenance_status: " + facility.getMaintenanceStatus();
                facility_maintenance.add(response);
            }
        } catch (Exception se) {System.err.println(se.getMessage());}
        return facility_maintenance;
    }

    public String getMaintenance(int facilityID) {
        String response = "";
        try {
            Maintenance facility = _maintenanceDAO.ReadMaintenance(facilityID);

            response =  "facility_id: "         + facility.getFacilityID() +
                        " facility_downtime: "  + facility.getFacilityDowntime() +
                        "\nmaintenance_cost: "   + facility.getMaintenanceCost() +
                        " maintenance_status: " + facility.getMaintenanceStatus();

        } catch(Exception se) {System.err.println(se.getMessage());}
        return response;
    }

    public String addMaintenance(Maintenance newObj) {
        try { _maintenanceDAO.Insert(newObj); }
        catch(Exception se) { System.err.println(se.getMessage());}
        return "success";
    }

    public String updateMaintenance(Maintenance maintenanceObj,int facilityID) {
        try { _maintenanceDAO.Update(maintenanceObj,facilityID); }
        catch(Exception se) { System.err.println(se.getMessage()); }
        return "success";
    }

    public String deleteMaintenance(int deleteID) {
        try { _maintenanceDAO.Delete(deleteID); }
        catch(Exception se) { System.err.println(se.getMessage()); }
        return "success";
    }

}