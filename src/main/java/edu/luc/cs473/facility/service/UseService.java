package edu.luc.cs473.facility.service;
import edu.luc.cs473.facility.dal.UseDAO;
import edu.luc.cs473.facility.models.use.FacilityUse;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by jlroo on 2/20/17.
 */

public class UseService {

    private UseDAO _useDAO = new UseDAO();

    public List<String> getAllOrders() {
        List<String> orders = new ArrayList<>();
        try {
            Collection<FacilityUse> AllOrders = _useDAO.ReadAllOrders();
            for (FacilityUse entry:AllOrders) {
                
                String response =   "facility_id: " + entry.getFacilityID() +
                                    " order_number: "+ entry.getOrderNumber() +
                                    "\ncustomer_id: "+ entry.getCustomerID() +
                                    " reservation_start: "+ entry.getReservationStart() +
                                    "\nreservation_end: "+ entry.getReservationEnd() +
                                    " inspection_date: "+ entry.getInspection().getInspectionDate();
                orders.add(response);
            }

        } catch (Exception se) {
            System.err.println(se.getMessage());
        }
        return orders;
    }

    public String getFacilityOrder(int facilityID) {
        String response = "";
        try {
            FacilityUse facility = _useDAO.ReadFacilityOrders(facilityID);

            response =  "facility_id: " + facility.getFacilityID() +
                        " order_number: "+ facility.getOrderNumber() +
                        "\ncustomer_id: "+ facility.getCustomerID() +
                        " reservation_start: "+ facility.getReservationStart() +
                        "\nreservation_end: "+ facility.getReservationEnd() +
                        " inspection_date: "+ facility.getInspection().getInspectionDate();
        } catch(Exception se) {
            System.err.println(se.getMessage());
        }
        return response;
    }

    public String addFacilityUse(FacilityUse newObj) {
        try { _useDAO.Insert(newObj); }
        catch(Exception se) {
            System.err.println(se.getMessage());
        }
        return "success";
    }

    public String updateFacilityUse(FacilityUse FacilityUseObj,int facilityID) {
        try { _useDAO.Update(FacilityUseObj,facilityID); }
        catch(Exception se) { System.err.println(se.getMessage()); }
        return "success";
    }

    public String deleteFacilityUse(int deleteID) {
        try { _useDAO.Delete(deleteID); }
        catch(Exception se) { System.err.println(se.getMessage()); }
        return "success";
    }
    
}