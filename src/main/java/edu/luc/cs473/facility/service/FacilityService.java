package edu.luc.cs473.facility.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import edu.luc.cs473.facility.dal.*;
import edu.luc.cs473.facility.models.facility.Facility;

public class FacilityService {

    private FacilityDAO _facilityDAO = new FacilityDAO();

    public List<String> getAllFacilities() {
        List<String> facilities = new ArrayList<>();
        try {
            Collection<Facility> AllFacilities = _facilityDAO.ReadAllFacilities();
            for (Facility entry:AllFacilities) {
                String response =   "facility_id: " + entry.getFacilityID() +
                                    " facility_name: "+ entry.getName() +
                                    " room_number: " + entry.getRoomNumber();
                facilities.add(response);
            }

        } catch (Exception se) {
            System.err.println(se.getMessage());
        }
        return facilities;
    }

    public String getFacility(int facilityID) {
        String response = "";
        try {
            Facility facility = _facilityDAO.ReadFacility(facilityID);
            response =   "facility_id: " + facility.getFacilityID() +
                                " facility_name: "+ facility.getName() +
                                " room_number: " + facility.getRoomNumber();
        } catch(Exception se) {
            System.err.println(se.getMessage());
        }
        return response;
    }

    public String addFacility(Facility newObj) {
        try { _facilityDAO.Insert(newObj); }
        catch(Exception se) { System.err.println(se.getMessage()); }
        return "success";
    }

    public String updateFacility(Facility newObj,int facilityID) {
        try { _facilityDAO.Update(newObj,facilityID); }
        catch(Exception se) { System.err.println(se.getMessage()); }
        return "success";
    }

    public String deleteFacility(int deleteID) {
        try { _facilityDAO.Delete(deleteID); }
        catch(Exception se) { System.err.println(se.getMessage()); }
        return "success";
    }

    public String systemClean() {
        try { _facilityDAO.cleanTables(); }
        catch(Exception se) { System.err.println(se.getMessage()); }
        return "success";
    }
}